import { Component } from '@angular/core';
import { OverlayService } from './overlay-module/service/overlay.service';
import { ExamplePopup } from './example-popup/example-popup.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-popup';

  constructor(public overlayService: OverlayService) {
  }

  openTestPopup() {
    let popup = this.overlayService.showPopup(ExamplePopup);
    popup.someDataPassedFromTheOutside = "эта строка передана в попап"
    
  }

}
