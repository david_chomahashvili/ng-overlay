import { ComponentRef } from '@angular/core';


export class Popup<T extends Popup<T>> {
    private thisCmpRef: ComponentRef<T>

    public setThisCmpRef(cmpRef: ComponentRef<T>): void {
        this.thisCmpRef = cmpRef;
    }

    public close() {
        this.thisCmpRef.destroy();
    }
}