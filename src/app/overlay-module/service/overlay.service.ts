import { Injectable, Type, ComponentRef } from '@angular/core';
import { OverlayContainer } from '../components/overlay-container/overlay-container.component';
import { Popup } from './Popup';

@Injectable({
  providedIn: 'root'
})
export class OverlayService {
    private overlayContainer: OverlayContainer;

    constructor() { }

    public showPopup<T extends Popup<T>>(popupType: Type<T>): T  {
        if (!this.overlayContainer) {
            throw "Invalid state! overlayContainer must be registered first.";
        }

        return this.overlayContainer.openComponentInPopup(popupType);
    }

    public registerOverlayHost(overlayContainer: OverlayContainer): void {
        this.overlayContainer = overlayContainer;
    }
}