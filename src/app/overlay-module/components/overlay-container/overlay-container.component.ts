import { Component, ViewChild, ViewContainerRef, OnInit, ComponentRef, ComponentFactoryResolver, Type, ComponentFactory } from '@angular/core';
import { OverlayService } from 'src/app/overlay-module/service/overlay.service';
import { Overlay } from '../overlay/overlay.component';
import { Popup } from 'src/app/overlay-module/service/Popup';

@Component({
  selector: 'overlay-container',
  templateUrl: './overlay-container.component.html',
  styleUrls: ['./overlay-container.component.scss']
})
export class OverlayContainer implements OnInit {
    @ViewChild("container", { read: ViewContainerRef }) container: ViewContainerRef;

    constructor(
        private componentResolver: ComponentFactoryResolver,
        private overlayService: OverlayService) {
    }

    public openComponentInPopup<T extends Popup<T>>(popupType: Type<T>): T {

        let overlayFactory = this.componentResolver.resolveComponentFactory(Overlay);
        let overlay = this.container.createComponent(overlayFactory);
        
        let popupFactory: ComponentFactory<T> = this.componentResolver.resolveComponentFactory<T>(popupType);
        let popup: ComponentRef<T> = this.container.createComponent(popupFactory);

        popup.instance.setThisCmpRef(popup);

        popup.onDestroy(() => {
            overlay.destroy();
        });

        return popup.instance;
    }

    ngOnInit(): void {
        this.overlayService.registerOverlayHost(this);
    }

}