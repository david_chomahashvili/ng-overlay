import { NgModule } from '@angular/core';
import { OverlayContainer } from './components/overlay-container/overlay-container.component';
import { Overlay } from './components/overlay/overlay.component';


@NgModule({
    declarations: [
        OverlayContainer,
        Overlay
    ],
    exports: [
        OverlayContainer
    ],
    imports: [
    ],
    providers: [],
    entryComponents: [
        Overlay
    ]
  })
  export class OverlayModule { }

  /*
    1. Register this module in the AppModule
    2. Add <overlay-container> to the end of the root template of your app
    3. Create your popup class extending Popup
    4. Add your popup class to entryComponents of your module
    5. Use OverlayService to create and show popup
   */