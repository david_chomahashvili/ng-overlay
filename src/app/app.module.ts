import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OverlayModule } from './overlay-module/overlay.module';
import { ExamplePopup } from './example-popup/example-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    ExamplePopup
  ],
  imports: [
    BrowserModule,
    OverlayModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ExamplePopup
  ]
})
export class AppModule { }
