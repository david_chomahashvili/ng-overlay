import { Component } from '@angular/core';
import { Popup } from '../overlay-module/service/Popup';


@Component({
    selector: 'example-popup',
    templateUrl: './example-popup.component.html',
    styleUrls: ['./example-popup.component.scss']
})
export class ExamplePopup extends Popup<ExamplePopup> {
    public someDataPassedFromTheOutside = "";

    onCloseBtnClick() {
        console.log("сохраняем какие-то данные перед закрытием")
        this.close()
    }
}